# Important! This is not approved by the Rust Foundation

![Logo](./src-tauri/icons/128x128%402x.png "Clipboard Mascot")

## Ferris Clipboard

I named the project "Ferris" Clipboard instead of mentioning Rust because I'm trying to avoid legal issues with the Rust foundation.

The main inspiration behind this project is that my company has a policy that made me switch to Windows from Arch linux. My company's GPO broke the native windows clipboard. Currently this clipboard is incredibly basic and lacks features. It has only been tested on Windows.

## How to use

Ferris Clipboard saves use clipboard input. It also allows you to save items to a separate history for easy quick access. Another feature is text masking. You can write hidden values to the settings section that allow you to overwrite the display of a string. E.g. if you set hidden items with a key of `foo` and a value of `bar` then every occurrence of `bar` will be display as `foo`. It will still copy to your clipboard correctly however. Currently the keybinding for this project is `Shift+space` and cannot be customized.

This project has only been tested on Windows and Mac OS X. Other platforms should still work.

<details>
  <summary>Screenshots</summary>
![image](/uploads/e9c90b3dd16f468f7edb0ee61d483a32/image.png)
![image](/uploads/80e752c550c92513ed62f8ea8fe6104c/image.png)
![image](/uploads/9a3d00231fa924f9d6d0c29c281cc547/image.png)
</details>



## Downloads

Downloads can be found at the [first official release](https://gitlab.com/bck01215/ferris-clipboard/-/releases/0.1.0).
