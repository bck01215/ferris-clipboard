/// Thanks  https://github.com/RandomEngy/tauri-sqlite
use rusqlite::{named_params, Connection, Statement};
use std::fs;
use tauri::AppHandle;

const CURRENT_DB_VERSION: u32 = 2;

/// Initializes the database connection, creating the .sqlite file if needed, and upgrading the database
/// if it"s out of date.
pub fn initialize_database(app_handle: &AppHandle) -> Result<Connection, rusqlite::Error> {
    let app_dir = app_handle
        .path_resolver()
        .app_data_dir()
        .expect("The app data directory should exist.");
    fs::create_dir_all(&app_dir).expect("The app data directory should be created.");
    let sqlite_path = app_dir.join("ferris_clipboard.sqlite");
    let mut db = Connection::open(sqlite_path)?;

    let mut user_pragma = db.prepare("PRAGMA user_version")?;
    let existing_user_version: u32 = user_pragma.query_row([], |row| row.get(0))?;
    drop(user_pragma);

    upgrade_database_if_needed(&mut db, existing_user_version)?;

    Ok(db)
}

/// Upgrades the database to the current version.
pub fn upgrade_database_if_needed(
    db: &mut Connection,
    existing_version: u32,
) -> Result<(), rusqlite::Error> {
    if existing_version < CURRENT_DB_VERSION {
        db.pragma_update(None, "journal_mode", "WAL")?;

        let tx = db.transaction()?;

        tx.pragma_update(None, "user_version", CURRENT_DB_VERSION)?;

        tx.execute_batch(
            "
      CREATE TABLE IF NOT EXISTS history (
        id INTEGER PRIMARY KEY,
        data_type TEXT NOT NULL,
        value BLOB NOT NULL
      );
      CREATE TABLE IF NOT EXISTS secrets (
        id INTEGER PRIMARY KEY,
        display TEXT NOT NULL,
        value BLOB NOT NULL
      );
      CREATE TABLE IF NOT EXISTS saved (
        id INTEGER PRIMARY KEY,
        data_type TEXT NOT NULL,
        value BLOB NOT NULL
      );
      ",
        )?;

        tx.commit()?;
    }

    Ok(())
}

pub fn add_item(data_type: &str, value: &str, db: &Connection) -> Result<(), rusqlite::Error> {
    let mut statement =
        db.prepare("INSERT INTO history (data_type, value) VALUES (@data_type, @value)")?;
    statement.execute(named_params! { "@data_type": data_type, "@value": value, })?;

    Ok(())
}

pub fn add_saved(data_type: &str, value: &str, db: &Connection) -> Result<(), rusqlite::Error> {
    let mut statement =
        db.prepare("INSERT INTO saved (data_type, value) VALUES (@data_type, @value)")?;
    statement.execute(named_params! { "@data_type": data_type, "@value": value, })?;

    Ok(())
}
pub fn delete_saved(value: &str, db: &Connection) -> Result<(), rusqlite::Error> {
    let mut statement = db.prepare("DELETE FROM saved WHERE value = @value")?;
    statement.execute(named_params! {  "@value": value, })?;

    Ok(())
}
pub fn add_hidden(display: &str, value: &str, db: &Connection) -> Result<(), rusqlite::Error> {
    let mut statement =
        db.prepare("INSERT INTO secrets (display, value) VALUES (@display, @value)")?;
    statement.execute(named_params! { "@display": display, "@value": value, })?;

    Ok(())
}
pub fn delete_hidden(value: &str, db: &Connection) -> Result<(), rusqlite::Error> {
    let mut statement = db.prepare("DELETE FROM secrets WHERE value = @value")?;
    statement.execute(named_params! {  "@value": value, })?;

    Ok(())
}

pub fn get_all(db: &Connection) -> Result<Vec<(String, String)>, rusqlite::Error> {
    let statement =
        db.prepare("SELECT DISTINCT data_type, value FROM history ORDER BY id DESC LIMIT 300")?;
    return_data(statement, "data_type", "value")
}
pub fn get_all_saved(db: &Connection) -> Result<Vec<(String, String)>, rusqlite::Error> {
    let statement =
        db.prepare("SELECT DISTINCT data_type, value FROM saved ORDER BY id DESC LIMIT 300")?;
    return_data(statement, "data_type", "value")
}
pub fn get_all_hidden(db: &Connection) -> Result<Vec<(String, String)>, rusqlite::Error> {
    let statement =
        db.prepare("SELECT DISTINCT display, value FROM secrets ORDER BY id DESC LIMIT 300")?;
    return_data(statement, "display", "value")
}

fn return_data(
    mut statement: Statement,
    col1: &str,
    col2: &str,
) -> Result<Vec<(String, String)>, rusqlite::Error> {
    let mut rows = statement.query([])?;
    let mut history = Vec::new();
    while let Some(row) = rows.next()? {
        let data: String = row.get(col1)?;
        let val: String = row.get(col2)?;

        history.push((data, val));
    }

    Ok(history)
}
