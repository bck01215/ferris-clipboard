// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]
mod database;
mod state;

use state::{AppState, ServiceAccess};
use tauri::{AppHandle, Manager, State};

use enigo::*;
// Learn more about Tauri commands at https://tauri.app/v1/guides/features/command
#[tauri::command]
fn get_coords() -> (i32, i32) {
    let enigo = Enigo::new();
    enigo.mouse_location()
}

#[tauri::command]
fn get_data(app_handle: AppHandle) -> std::vec::Vec<(std::string::String, std::string::String)> {
    app_handle.db(database::get_all).unwrap_or_default()
}
#[tauri::command]
fn get_saved(app_handle: AppHandle) -> std::vec::Vec<(std::string::String, std::string::String)> {
    app_handle.db(database::get_all_saved).unwrap_or_default()
}
#[tauri::command]
fn get_hidden(app_handle: AppHandle) -> std::vec::Vec<(std::string::String, std::string::String)> {
    app_handle.db(database::get_all_hidden).unwrap_or_default()
}
#[tauri::command]
fn delete_saved(
    app_handle: AppHandle,
    value: &str,
) -> std::vec::Vec<(std::string::String, std::string::String)> {
    app_handle
        .db(|db| database::delete_saved(value, db))
        .unwrap();

    get_saved(app_handle)
}

#[tauri::command]
fn delete_hidden(
    app_handle: AppHandle,
    value: &str,
) -> std::vec::Vec<(std::string::String, std::string::String)> {
    app_handle
        .db(|db| database::delete_hidden(value, db))
        .unwrap();

    get_hidden(app_handle)
}

#[tauri::command]
fn write_data(
    app_handle: AppHandle,
    data_type: &str,
    value: &str,
) -> std::vec::Vec<(std::string::String, std::string::String)> {
    app_handle
        .db(|db| database::add_item(data_type, value, db))
        .unwrap();

    get_data(app_handle)
}
#[tauri::command]
fn write_saved(
    app_handle: AppHandle,
    data_type: &str,
    value: &str,
) -> std::vec::Vec<(std::string::String, std::string::String)> {
    app_handle
        .db(|db| database::add_saved(data_type, value, db))
        .unwrap();

    get_saved(app_handle)
}
#[tauri::command]
fn write_hidden(
    app_handle: AppHandle,
    display: &str,
    value: &str,
) -> std::vec::Vec<(std::string::String, std::string::String)> {
    app_handle
        .db(|db| database::add_hidden(display, value, db))
        .unwrap();

    get_hidden(app_handle)
}
fn main() {
    tauri::Builder::default()
        .manage(AppState {
            db: Default::default(),
        })
        .invoke_handler(tauri::generate_handler![
            get_coords,
            get_data,
            write_data,
            get_saved,
            write_saved,
            delete_saved,
            get_hidden,
            write_hidden,
            delete_hidden
        ])
        .setup(|app| {
            let handle = app.handle();

            let app_state: State<AppState> = handle.state();
            let db =
                database::initialize_database(&handle).expect("Database initialize should succeed");
            *app_state.db.lock().unwrap() = Some(db);

            Ok(())
        })
        .plugin(tauri_plugin_clipboard::init())
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
